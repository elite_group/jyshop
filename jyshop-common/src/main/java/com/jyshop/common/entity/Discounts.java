package com.jyshop.common.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhao
 * @since 2020-05-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("jy_discounts")
public class Discounts implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * spuid
     */
    private String name;

    /**
     * spu的减免
     */
    private Integer spuId;

    /**
     * 领取时间
     */
    private LocalDate startDate;

    /**
     * 到期时间
     */
    private LocalDate endDate;

    /**
     * 减免金额
     */
    private BigDecimal minusPrice;


}
