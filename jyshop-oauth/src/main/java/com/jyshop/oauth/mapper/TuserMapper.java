package com.jyshop.oauth.mapper;

import com.jyshop.oauth.entity.Tuser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjq
 * @since 2020-05-21
 */
public interface TuserMapper extends BaseMapper<Tuser> {

}
