package com.jyshop.oauth.service;

import com.jyshop.oauth.entity.Tuser;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjq
 * @since 2020-05-21
 */
public interface ITuserService extends IService<Tuser> {

}
